#include "table.h"

Table Table_new( int rows, int cols ) {

    // allocate table
    Table table = (Table)malloc( rows * sizeof( float * ) );
    int x;
    for( x = 0; x < rows; x++ ) {
        table[x] = malloc( cols * sizeof( float ) );

        // populate with random numbers
        int y;
        for( y = 0; y < cols; y++ ) {
            float rnd = rand();
            table[x][y] = (float) rnd;
        }
    }

    return table;
}

void Table_free( Table table, int rows, int cols ) {
    int x;
    for( x = 0; x < rows; x++ ) {
        free( table[x] );
    }
    
    free( table );
}

void Table_display( Table table, int rows, int cols ) {
    // printf the table
    printf( "\n\r    x     |     y\n\r---------------------\n\r" );

    int x, y;
    for( x = 0; x < rows; x++ ) {
        for( y = 0; y < cols; y++ ) {
            if( y % 2 == 0 )    printf( "%f | ",  table[x][y] );
            else                printf( "%f\r\n", table[x][y] );
        }
    }
}

void Table_sort_2D_by_2nd_col( Table table, int rows, int cols, sortOrder order ) {
    order == ASC ? 
        qsort( table, rows, cols * sizeof( float ), Table_element_cmp_2D_by_2nd_col_asc )
        :
        qsort( table, rows, cols * sizeof( float ), Table_element_cmp_2D_by_2nd_col_desc );
}

int Table_element_cmp_2D_by_2nd_col_asc( const void *ax, const void *bx ) {
    float a = ((float*)ax)[1];
    float b = ((float*)bx)[1];

    return a < b ? -1 : ( a > b );
}

int Table_element_cmp_2D_by_2nd_col_desc( const void *ax, const void *bx ) {
    float a = ((float*)ax)[1];
    float b = ((float*)bx)[1];

    return b < a ? -1 : ( b > a );
}

float *Table_getCol( Table table, int rows, int cols, int columnIndex ) {
    if( columnIndex > cols - 1 ) {
        fprintf( stderr, "Table_getCol: chosen 'columnIndex' > number of columns." );
        assert( 1 );
    }

    float *arr = (float*)malloc( rows * sizeof( float ) );

    int x;
    for( x = 0; x < rows; x++ ) {
        arr[x] = table[x][columnIndex];
    }

    return arr;
}