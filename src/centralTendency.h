#include <math.h>
#include <stdlib.h>
#include "table.h"

typedef struct modeFloatStruct {
    int count;
    float *arr;
} modeFloatStruct;

float mean( float *arr, int count );

float median( float *arr, int count );

modeFloatStruct mode( float *arr, int count );

float *dupFloatArr( float *arr, int count );

int cmpFloats( const void *a, const void *b );