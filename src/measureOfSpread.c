#include "measureOfSpread.h"

float sampleVariance( float *arr, int count ) {
    float sumOfSqrdDiffFromMean = 0;
    float _mean = mean( arr, count );

    int x;
    for( x = 0; x < count; x++ ) {
        sumOfSqrdDiffFromMean += squaredDiffFromMean( arr[x], _mean );
    }

    return sumOfSqrdDiffFromMean / ( count - 1 );
}

float sampleStandardDeviation( float *arr, int count ) {
    return sqrt( sampleVariance( arr, count ) );
}

float squaredDiffFromMean( float x, float mean ) {
    return pow( x - mean, 2 );
}