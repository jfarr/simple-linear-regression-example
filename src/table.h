#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

typedef float ** Table;
typedef enum sortOrder {
    ASC = 0,
    DESC = 1
} sortOrder;

Table Table_new( int rows, int cols );

void Table_free( Table table, int rows, int cols );

void Table_display( Table table, int rows, int cols );

void Table_sort( Table table, int rows, int cols, sortOrder order );
void Table_sort_2D_by_2nd_col( Table table, int rows, int cols, sortOrder order );
int Table_element_cmp_2D_by_2nd_col_asc( const void *a, const void *b );
int Table_element_cmp_2D_by_2nd_col_desc( const void *a, const void *b );

float *Table_getCol( Table table, int rows, int cols, int columnIndex );