#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
// #include "table.h"
// #include "centralTendency.h"
#include "measureOfSpread.h"

void slr();

float linearRegression( float x, Table table, int rows, int cols );

float slope( Table table, int rows, int cols);

float yIntercept( Table table, int rows, int cols );

float correlationCoefficient( Table table, int rows, int cols );

float sumOfSquares( float *arr, int count );