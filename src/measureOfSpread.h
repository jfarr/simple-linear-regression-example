#include <math.h>
#include "centralTendency.h"

float sampleVariance( float *arr, int count );

float sampleStandardDeviation( float *arr, int count );

float squaredDiffFromMean( float x, float mean );