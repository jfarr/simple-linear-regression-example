#include "centralTendency.h"

float mean( float *arr, int count ) {
	float sum = 0;

    int x;
    for( x = 0; x < count; x++ ) {
        sum += arr[x];
    }

    return sum / count;
}

float median( float *arr, int count ) {
    if( count % 2 == 0 ) {
        // even
        int index = count / 2;
        return arr[index] + arr[index + 1] / 2;
    } else {
        // odd
        int index = ceil( count / 2 );
        return arr[index];
    }
}

modeFloatStruct mode( float *arr, int count ) {
    float *dup = dupFloatArr( arr, count );
    qsort( dup, count, sizeof( float ), cmpFloats );

    Table tbl = Table_new( count, 2 );

    int countOfValueAtIndex;
    float valueAtIndex;
    int x;
    for( x = 0; x < count; x++ ) {
        if( x = 0 ) {
            valueAtIndex = arr[x];
            countOfValueAtIndex = 1;
        } else {
            if( valueAtIndex == arr[x] ) {
                countOfValueAtIndex++;
            } else {
                valueAtIndex = arr[x];
                countOfValueAtIndex = 1;
            }
        }
    }

    // sort table by count then set result with the highest value or values.
    Table_sort_2D_by_2nd_col( tbl, count, 2, DESC );
    int modalCount = 1;
    int temp;
    for( x = 0; x < count; x++ ) {
        if( x == 0 ) {
            temp = tbl[x][1];
        } else {
            if( tbl[x][1] == temp ) {
                modalCount++;
            } else {
                break;
            }
        }
    }

    modeFloatStruct result;
    result.count = modalCount;
    result.arr = (float*)malloc( modalCount * sizeof( float ) );

    for( x = 0; x < modalCount; x++ ) {
        result.arr[x] = tbl[x][0];
    }
    
    Table_free( tbl, count, 2 );

    return result;
}

float *dupFloatArr( float *arr, int count ) {
    float *dup = (float*)malloc( count * sizeof( float ) );

    int x;
    for( x = 0; x < count; x++ ) {
        dup[x] = arr[x];
    }

    return dup;
}

int cmpFloats( const void *ax, const void *bx ) {
    float a = *(float*)ax;
    float b = *(float*)bx;

    return a < b ? -1 : ( a > b );
}