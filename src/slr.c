#include "slr.h"

void slr() {
    srand( time( NULL ) );
	
	printf( "Simple Linear Regression:\n\r" );

	const int rows = 10;
	const int cols = 2;
	Table sample = Table_new( rows, cols );

	// Use this to set specific values in the table:
	sample[0][0] = 17;
	sample[0][1] = 94;
	sample[1][0] = 13;
	sample[1][1] = 73;
	sample[2][0] = 12;
	sample[2][1] = 59;
	sample[3][0] = 15;
	sample[3][1] = 80;
	sample[4][0] = 16;
	sample[4][1] = 93;
	sample[5][0] = 14;
	sample[5][1] = 85;
	sample[6][0] = 16;
	sample[6][1] = 66;
	sample[7][0] = 16;
	sample[7][1] = 79;
	sample[8][0] = 18;
	sample[8][1] = 77;
	sample[9][0] = 19;
	sample[9][1] = 91;

	Table_display( sample, rows, cols );


	float *xArr = Table_getCol( sample, rows, cols, 0 );
	float *yArr = Table_getCol( sample, rows, cols, 1 );
	printf( "\n\rmean X: %f\n\r", mean( xArr, rows ) );
	printf( "mean Y: %f\n\r", mean( yArr, rows ) );
	printf( "sum of squares X: %f\n\r", sumOfSquares( xArr, rows ) );
	printf( "sum of squares Y: %f\n\r", sumOfSquares( yArr, rows ) );
	printf( "sample variance X: %f\n\r", sampleVariance( xArr, rows ) );
	printf( "sample variance Y: %f\n\r", sampleVariance( yArr, rows ) );
	printf( "sample standard deviation X: %f\n\r", sampleStandardDeviation( xArr, rows ) );
	printf( "sample standard deviation Y: %f\n\r", sampleStandardDeviation( yArr, rows ) );
	free( xArr );
	free( yArr );

	printf( "correlation coefficient: %f\n\r", correlationCoefficient( sample, rows, cols ) );
	printf( "slope: %f\n\r", slope( sample, rows, cols ) );
	printf( "y-intercept: %f\n\r", yIntercept( sample, rows, cols ) );


	// Given x calculate y
	float finput = 5;
	printf( "\n\rif x is %f then y is %f\n\r", finput, linearRegression( finput, sample, rows, cols ) );
	finput = 10;
	printf( "if x is %f then y is %f\n\r", finput, linearRegression( finput, sample, rows, cols ) );
	finput = 25;
	printf( "if x is %f then y is %f\n\r\n\r\n\r", finput, linearRegression( finput, sample, rows, cols ) );

	Table_free( sample, rows, cols );
}

float linearRegression( float x, Table table, int rows, int cols ) {
	// y = a + bx
	return yIntercept( table, rows, cols ) + ( slope( table, rows, cols ) * x );
}

float slope( Table table, int rows, int cols ) {
	float *xArr = Table_getCol( table, rows, cols, 0 );
	float *yArr = Table_getCol( table, rows, cols, 1 );
	
	// b = r( Sy / Sx ) -- where b is slope of regression line
	float b = correlationCoefficient( table, rows, cols ) * ( sampleStandardDeviation( yArr, rows ) / sampleStandardDeviation( xArr, rows ) );

	free( xArr );
	free( yArr );
	
	return b;
}

float yIntercept( Table table, int rows, int cols ) {
	float *xArr = Table_getCol( table, rows, cols, 0 );
	float *yArr = Table_getCol( table, rows, cols, 1 );

	// a = mean(y) - b(mean(x)) -- where a is y-intercept of regression line
	float a = mean( yArr, rows ) - ( slope( table, rows, cols ) * mean( xArr, rows ) );

	free( xArr );
	free( yArr );

	return a;
}

float correlationCoefficient( Table table, int rows, int cols ) {
	float *xArr = Table_getCol( table, rows, cols, 0 );
	float *yArr = Table_getCol( table, rows, cols, 1 );
	float meanX = mean( xArr, rows );
	float meanY = mean( yArr, rows );

	// r = sum( (x - mean(x)) (y - mean(y)) ) / sqrt( sum(pow( x - mean(x) )) * sum(pow( y - mean(y) )) )
	float sum = 0;
	float soqX = 0;
	float soqY = 0;
	int index;
	for( index = 0; index < rows; index++ ) {
		sum += ( xArr[index] - meanX ) * ( yArr[index] - meanY );
		/*
		printf( "(x-mean(x)): %f\n\r", ( xArr[index] - meanX ) );
		printf( "(y-mean(y)): %f\n\r", ( yArr[index] - meanY ) );
		printf( "(x-mean(x))*(y-mean(y)): %f\n\r", ( xArr[index] - meanX ) * ( yArr[index] - meanY ) );
		*/
	}
	// printf( "sum((x-mean(x))*(y-mean(y))): %f\n\r", sum );

	soqX += sumOfSquares( xArr, rows );
	soqY += sumOfSquares( yArr, rows );

	free( xArr );
	free( yArr );

	return sum / sqrt( soqX * soqY );
}

float sumOfSquares( float *arr, int count ) {
	float _mean = mean( arr, count );

	float sum = 0;
	int x;
	for( x = 0; x < count; x++ ) {
		sum += pow( arr[x] - _mean, 2 );
	}

    return sum;
}