# Simple Linear Regression Example

Simple Linear Regression Example written in C.

This is a very simple example of performing simple linear regression. This was made to demonstrate that I can understand and perform the math required for linear regression. This doesn't incorporate all the required elements of regression analysis; such as finding the p value to reject the null hypothesis. Which of course finding correllation between multiple variables is the whole point of performing linear regression. Again this is a simple example of my ability to understand and do the math not an application of linear regression for regression analysis.

This also doesn't show multiple linear regression.